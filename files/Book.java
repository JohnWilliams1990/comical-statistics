
package homework2;

public class Book {

    private String name = "";
    private int numberOfCharacters = 0;
        
    public String getName()
    {
        return name;
    }
    public int getNumberOfCharacters()
    {
        return numberOfCharacters;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    public void setNumberOfCharacters(int numberOfCharacters)
    {
        this.numberOfCharacters = numberOfCharacters;
    }
   

       //  MAKE AN INCRAMENTTAL FUNCTION FOR THE NUMBEROFCHARACTERS
     public void  INCnumberOfCharacters()
    {
        numberOfCharacters++;
    }
    
            
    
}
