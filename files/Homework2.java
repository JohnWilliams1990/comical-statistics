// John Williams
//105201054
//Principals of programming - programming assignment 2

//  this program has a few assumptions that enable us to look at the data in a different manner
//  a graph is a set of distinct nodes and a set of distinct edges. because of this we can read in the 
//  nopdes.csv file with confidence that there are no two repeated characters or comic books, that are 
//  exactly the same name. from this assuption we see that it is a simple matter to count the books and 
//  characters, and then to count the apearances in a book for a specific character and characters in a 
//  specific book
//


package homework2;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.*;

public class Homework2 {
    static ArrayList<Character> characters =  new ArrayList<>(); 
    static ArrayList<Book> comicBook =  new ArrayList<>();
    static Character tempCharacter = new Character();
    static Book tempComicBooks  = new Book();
    static float numberOfHeros = 0;
    static float numberOfComics = 0;
    static float avgNumberOfHerosPerComic = 0;
    static float avgNumberOfComicsPerHeros = 0;
    static float avgNumberOfpartners = 0;
    
    
    //
    //  Description: this main function reads three files and determines whether the item read from nodes.csv is a Hero or a Comic. 
    //  From there it makes a ComicBook item to store the Comic book or a Character item to house the character and puts it in an 
    //  ArrayList of the respective type. I did this to parse out repeated Heros or Comics, if they existed. I admit that is was unlikely 
    //  that there would be a repeated item in the set of Heros or Comics, because of what the definition of a graph is. Yet this dosent 
    //  preclude human error. I initially started out looking to do direct comparisons of the elemetnts using the .contains() of the 
    //  item in the array list and read in each file and do the regex for all of them. then Dr. Williams suggested that we 
    //  use the definition of waht a graph is and suggested the subtle idea of deviding the sum of one by the other. 
    
    
    public static void main(String[] args)   throws FileNotFoundException {
       
        Scanner scannerForFile = new Scanner(new File("/Users/John/Documents/NetBeansProjects/Homework2/src/Homework2/nodes.csv"));
        String [] Answer;
        int i = 0;        // counter to count all of the items within the loop
        String temp = "";// temporary string for holding the parsed term
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Read in nodes.csv and if character then add to character arraylist else add to comic array list
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        while(scannerForFile.hasNext()){
            temp = scannerForFile.nextLine();// get next line in the file
           
            Answer = getExpression("[\"]{1}(\\w|\\s|\\W|[\\,]){0,}[\"]{1}[\\,]{0,}|"+// // parse all first and second that have ("") or 
                    "(\\w|['/.&-]|\\s|\\[|\\]|\\||\\(|\\)|\\{|\\}|;|:|<|>|\\?){0,}|"+// parse all first and second that dont have ("") or 
                    "(\\w|\\s|\\W|\\s){0,}|" ,temp);//parses all simple words without quotations

            
            
                if (Answer[1].contentEquals("hero"))// if its a hero
                {    
                    Character tempCharacter = new Character();
                    tempCharacter.setName(Answer[0]);
                    if (!characters.contains(tempCharacter));   // if the character dosen't exist add it to the arraylist
                    characters.add(tempCharacter);              
                }

                else if (Answer[1].contentEquals("comic"))// if its a comic book 
                {
                    Book tempComicBooks  = new Book();
                    tempComicBooks.setName(Answer[0]);
                    if (!comicBook.contains(tempComicBooks));// if the book dosen't exist add it to the arraylist
                    comicBook.add(tempComicBooks);                    
                }
//            }
            i= i+1;
        }
        
        numberOfHeros = characters.size();// determine number of characters from array 
        numberOfComics = comicBook.size();// determine number of comics from array 
        
        
        System.out.println("There are "+ (int)numberOfComics+" comic books.");
        System.out.println("There are "+ (int)numberOfHeros+" Heros.");
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Read in edges.csv and count the elements
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        Scanner scannerForFile1 = new Scanner(new File("/Users/John/Documents/NetBeansProjects/Homework2/src/Homework2/edges.csv"));
        i = 0;        // reset the interger i to 0
        temp = "";    // reset string value
        while(scannerForFile1.hasNext()){// while there is still stuf in the file to be read
            temp = scannerForFile1.nextLine();
           //System.out.println(temp);
           
            Answer = getExpression("[\"]{1}(\\w|\\s|\\W|[\\,]){0,}[\"]{1}[\\,]{0,}|"+// // parse all first and second that have ("")
                    "(\\w|['/.&-]|\\s|\\[|\\]|\\||\\(|\\)|\\{|\\}|;|:|<|>|\\?){0,}|"+// parse all first and second that dont have ("")
                    "(\\w|\\s|\\W|\\s){0,}|" ,temp);
            
            //System.out.println(Answer[0] +"~"+ Answer[1]);            
                   
            
            i= i+1;// count the number of Heros and characters
          
        }
        // here edges connect a hero to a comic book. because of this and the fact that the edges are in a set we can just divide through the total 
        // number of edges by either the Number of Comics or the number of heros to get the Number Of Heros Per Comic or Number Of Comics Per Heros, respectively. 
        // I could check to see if there are repeats, but after doing so I found there to be a negligable difference. 
           // I subtract one because there is the header on the file telling us wqhat is in the file
            avgNumberOfHerosPerComic = (i-1)/numberOfComics;// the number of Characters per comic is the number of Heros divided by the total number of edges 
            avgNumberOfComicsPerHeros = (i-1)/numberOfHeros;// the number of comics that a heros appears in is the number of comics divided by the total number of edges 

            
            System.out.println("The average number of characters per comic are "+avgNumberOfHerosPerComic+".");
            System.out.println("The average number of comics per are character are "+avgNumberOfComicsPerHeros+".");

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Read in hero-network.csv and count the number of partners
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Scanner scannerForFile2 = new Scanner(new File("/Users/John/Documents/NetBeansProjects/Homework2/src/Homework2/hero-network.csv"));
        i = 0;        
        temp = "";
        while(scannerForFile2.hasNext()){
           temp = scannerForFile2.nextLine();// get next line in the file
           //System.out.println(temp);
           
           Answer =  getExpression(// the regex string of possable combinations
                    "[\"]{1}(\\w|\\s|\\W|[\\,]){0,}[\"]{1}[\\,]{1}|"+// // parse all first and second that have ("")
                    "[\"]{1}(\\w|\\s|\\W|[\\,]){0,}[\"]{1}|"+// // parse all first and second that have ("")
                    "(\\w|\\s|\\W|\\s){0,}|" ,temp);
            //System.out.println(Answer[0] +"~"+ Answer[1]);            
                        
            i= i+1;// incrament the counter
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // The average number of partners 
        // The average number of partners is calculated by taking the set of partners and dividing it by the number of heros
        // I subtract one because there is the header on the file telling us wqhat is in the file
        avgNumberOfpartners = (i-1)/numberOfHeros;
        System.out.println("The average number of partners per character is "+ avgNumberOfpartners);        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  Description: this function uses the regex pattern to check for the specific pattern of the string that I am looking for.
    //  This function is designed to remove commas, quotes and shave off the extra spaces in the expression at the end.
    //  Output: two strings in a string array, one for each coloumn of information in the file. 
    //  Input: the regex patter that is being searched for, as a string, and the origional string. 
    //
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public static String[] getExpression(String Regex, String OriginalString)
    { 
        String[] toBeReturned = {"",""};// decalse array with two empt string values
              Pattern checher = Pattern.compile(Regex);// check for the pattern in the string 
            Matcher matcher = checher.matcher(OriginalString);// match the paturn to the existing string 
    
            while(matcher.find())
            {
                if (matcher.group().length() != 0)// if the string has length
                    {
                         if (matcher.group().endsWith(","))// if the group ends in a coma then it is our first part of the string
                        {
                            if (toBeReturned[0].length() ==0)// if the first string is empty then fill 
                            {
                            toBeReturned[0] = matcher.group().trim().replaceAll("\\,", " ".trim()).replaceAll("\"", " ".trim()).trim();// parse out and return only the part of the string that is relavent
                            }
                            else if (toBeReturned[1].length() ==0)// otherwise there is a repeat so fill the second string 
                            {
                                toBeReturned[1] = matcher.group().trim().replaceAll("\\,", " ".trim()).replaceAll("\"", " ".trim()).trim();// parse out and return only the part of the string that is relavent
                            }
                        }

                        else
                        {
                            if (toBeReturned[0].length() ==0)// otherwise we have the second string part that follows the comma delimeter
                            {
                            toBeReturned[0] = matcher.group().trim().replaceAll("\\,", " ".trim()).replaceAll("\"", " ".trim()).trim();// parse out and return only the part of the string that is relavent
                            }
                            else if (toBeReturned[1].length() ==0)// otherwise there is a repeat so fill the second string
                            {
                            toBeReturned[1] = matcher.group().trim().replaceAll("\\,", " ".trim()).replaceAll("\"", " ".trim().trim());// parse out and return only the part of the string that is relavent
                            }                           
                        }
                    }
            }
        return toBeReturned;// return the array of strings
    }
    
    
}