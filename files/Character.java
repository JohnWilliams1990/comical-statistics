
package homework2;

public class Character {
    private String name = "";
    private int BookAppearances = 0;
    private int numberOfPartners = 0;
    public String getName()
    {
        return name;
    }
    public int getBookAppearances()
    {
        return BookAppearances;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public void setBookAppearances(int BookAppearances)
    {
        this.BookAppearances = BookAppearances;
    }
    public void INCBookAppearances()
    {
        this.BookAppearances++;
    }
    public void setNumberOfPartners(int number)
    {
    numberOfPartners = number;
    }
    public void incramentNumberOfPartners()
    {
    numberOfPartners++;
    }
    public int getNumberOfPartners()
    {
    return numberOfPartners; 
    }
}
