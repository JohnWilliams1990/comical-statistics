// John Williams
//105201054
//Principals of programming - programming assignment 2

//  this program has a few assumptions that enable us to look at the data in a different manner
//  a graph is a set of distinct nodes and a set of distinct edges. because of this we can read in the 
//  nopdes.csv file with confidence that there are no two repeated characters or comic books, that are 
//  exactly the same name. from this assuption we see that it is a simple matter to count the books and 
//  characters, and then to count the apearances in a book for a specific character and characters in a 
//  specific book
//
//
//
//
//
//


package homework2;



import java.io.BufferedWriter;
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.regex.*;

public class Homework2 {
    static ArrayList<Character> characters =  new ArrayList<>(); 
    static ArrayList<Book> comicBook =  new ArrayList<>();
    static ArrayList<Partner> PartnersInCrime =  new ArrayList<>();
    static Character tempCharacter = new Character();
    static Book tempComicBooks  = new Book();
    static Partner tempPartner = new Partner();        
    static float numberOfHeros = 0;
    static float numberOfComics = 0;
    static float avgNumberOfHerosPerComic = 0;
    static float avgNumberOfComicPerHeros = 0;
    static float avgNumberOfpartners = 0;
    
    
    public static void main(String[] args)   throws FileNotFoundException {
        
        
       
        
        
        Scanner scannerForFile = new Scanner(new File("/Users/John/Documents/NetBeansProjects/Homework2/src/Homework2/nodes.csv"));
        String [] Answer;
        int i = 0;        
        String temp = "";
        while(scannerForFile.hasNext()){
           // System.out.print(i+":  ");
            temp = scannerForFile.nextLine();
           // System.out.println(temp);
           // System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
           
            Answer = getExpression("[\"]{1}(\\w|\\s|\\W|[\\,]){0,}[\"]{1}[\\,]{0,}|"+// // parse all first and second that have ("")
                    "(\\w|['/.&-]|\\s|\\[|\\]|\\||\\(|\\)|\\{|\\}|;|:|<|>|\\?){0,}|"+// parse all first and second that dont have ("")
                            //"(\\w{0,}|\\s{0,}|\\W{0,}|\\s{0,}){0,}|" ,temp);
                    "(\\w|\\s|\\W|\\s){0,}|" ,temp);
            
             if (i!=0)
            {
                if (Answer[1].contentEquals("hero"))
                {    
                Character tempCharacter = new Character();
                tempCharacter.setName(Answer[0]);
                if (!characters.contains(tempCharacter));
                characters.add(tempCharacter);
                    numberOfHeros++;
     
                }

                else if (Answer[1].contentEquals("comic"))
                {
                Book tempComicBooks  = new Book();
                    tempComicBooks.setName(Answer[0]);
                    if (!comicBook.contains(tempComicBooks));
                    comicBook.add(tempComicBooks);
                    numberOfComics++;
                }
            }
            
            
           // System.out.println(Answer[0] +"~"+ Answer[1]);
            
            //System.out.println();
           // System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            i= i+1;
        }
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        Scanner scannerForFile1 = new Scanner(new File("/Users/John/Documents/NetBeansProjects/Homework2/src/Homework2/edges.csv"));
        i = 0;        
        temp = "";
        while(scannerForFile1.hasNext()){
            //System.out.print(i+":  ");
            temp = scannerForFile1.nextLine();
           //System.out.println(temp);
           // System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
           
            Answer = getExpression("[\"]{1}(\\w|\\s|\\W|[\\,]){0,}[\"]{1}[\\,]{0,}|"+// // parse all first and second that have ("")
                    "(\\w|['/.&-]|\\s|\\[|\\]|\\||\\(|\\)|\\{|\\}|;|:|<|>|\\?){0,}|"+// parse all first and second that dont have ("")
                    "(\\w|\\s|\\W|\\s){0,}|" ,temp);
            
          
            //System.out.println(Answer[0] +"~"+ Answer[1]);
            tempCharacter = new Character();
            tempCharacter.setName(Answer[0]);
            if (i!=0)
            {
                for(Character C : characters){

                    if(C.getName().contentEquals(Answer[0]))//.contains(Answer[0]))
                    {
                    C.INCBookAppearances();
                   // break;
                    }

                }
                for(Book B : comicBook){
                    if(B.getName().contentEquals(Answer[1]))
                    {
                    B.INCnumberOfCharacters();
                   // break;
                    }
                }
            }
            
            
           // System.out.println();
           // System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            i= i+1;
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Scanner scannerForFile2 = new Scanner(new File("/Users/John/Documents/NetBeansProjects/Homework2/src/Homework2/hero-network.csv"));
        i = 0;        
        temp = "";
        while(scannerForFile2.hasNext()){
           System.out.print(i+":  ");
           temp = scannerForFile2.nextLine();
           //System.out.println(temp);
           //System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
           
           Answer =  getExpression(
                    "[\"]{1}(\\w|\\s|\\W|[\\,]){0,}[\"]{1}[\\,]{1}|"+// // parse all first and second that have ("")
                    "[\"]{1}(\\w|\\s|\\W|[\\,]){0,}[\"]{1}|"+// // parse all first and second that have ("")
                    
                    //"(\\w{0,}|\\s{0,}|\\W{0,}|\\s{0,}){0,}|" ,temp);
                    "(\\w|\\s|\\W|\\s){0,}|" ,temp);

            
          
            System.out.println(Answer[0] +"~"+ Answer[1]);
            
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//269776:  "BINARY/CAROL DANVERS","IRON MAN/TONY STARK "-----------> the space can be AVOIDED via .contains() instead of .contentEquals()
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//BINARY/CAROL DANVERS~IRON MAN/TONY STARK 


//            tempCharacter = new Character();
//            tempCharacter.setName(Answer[0]);


            if (i!=0)
            {
                for(Character C : characters){
                            //via extra spaces we should use contains here STARSHINE II/BRANDY
                            // i don't think this works... it mostly works but not for all cases
                    if (Answer[0].endsWith(" "))
                    {
                    Answer[0].trim();
                    }
                    else if (Answer[1].endsWith(" "))
                    {
                    Answer[1].trim();
                    }
                    if(C.getName().contains(Answer[0]))//.contains(Answer[0]))
                    {
                        C.incramentNumberOfPartners();
                    }
                     
                }
               for(Character C : characters){

                    if(C.getName().contains(Answer[1]))//.contains(Answer[0]))
                    {
                        C.incramentNumberOfPartners();
                    }
                     
                }
            }

            
           // System.out.println();
          //  System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            i= i+1;
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


           System.out.println("~~~~~~~~~~~~characters~~~~~~~~~~~~~"+characters.size());
       Iterator<Character> CharIterator = characters.iterator();

       
       
       
       while(CharIterator.hasNext())
        {
            Character item = CharIterator.next();
            System.out.println(item.getName()+" -->"+item.getBookAppearances()+" -->"+item.getNumberOfPartners());
        }
        Iterator<Book> BookIterator = comicBook.iterator();
            System.out.println("~~~~~~~~~~~~books~~~~~~~~~~~~~"+comicBook.size());
        
        while(BookIterator.hasNext())
        {
            Book itemB = BookIterator.next();
            System.out.println(itemB.getName()+" -->"+itemB.getNumberOfCharacters());
        }
        
        
        
        

    }
    
    public static String[] getExpression(String Regex, String OriginalString)
    {
        boolean taken1 = false;
        boolean taken2 = false;
        
        String[] toBeReturned = {"",""};
              Pattern checher = Pattern.compile(Regex);
            Matcher matcher = checher.matcher(OriginalString);
    
            while(matcher.find())
            {
                if (matcher.group().length() != 0)
                    {
                         if (matcher.group().endsWith(","))
                        {

                        // 2: System.out.println(regexMatcher.group().trim().replace(',', ' ').trim().replaceAll("\"", " ".trim()));
                        // 1: System.out.println(regexMatcher.group().trim().replace(',', ' '));
                         // 3: FINAL -->    System.out.println(regexMatcher.group().trim().replaceAll("\\,", " ".trim()).replaceAll("\"", " ".trim()));
                          //  System.out.println("oPTION1");
                           //// System.out.println(matcher.group().trim().replaceAll("\\,", " ".trim()).replaceAll("\"", " ".trim()));
                            if (toBeReturned[0].length() ==0)
                            {
                            toBeReturned[0] = matcher.group().trim().replaceAll("\\,", " ".trim()).replaceAll("\"", " ".trim()).trim();
                            }
                            else if (toBeReturned[1].length() ==0)
                            {
                                toBeReturned[1] = matcher.group().trim().replaceAll("\\,", " ".trim()).replaceAll("\"", " ".trim()).trim();
                            }
                        }

                        else
                        {
                        //     System.out.println(regexMatcher.group().trim().replaceAll("\\,", " ".trim()).replaceAll("\"", " ".trim()));
                         //   System.out.println("oPTION2");
                           //// System.out.println(matcher.group().trim());
                            if (toBeReturned[0].length() ==0)
                            {
                            toBeReturned[0] = matcher.group().trim().replaceAll("\\,", " ".trim()).replaceAll("\"", " ".trim()).trim();
                            }
                            else if (toBeReturned[1].length() ==0)
                            {

                            toBeReturned[1] = matcher.group().trim().replaceAll("\\,", " ".trim()).replaceAll("\"", " ".trim().trim());
//                            toBeReturned[1] = matcher.group().trim();
                            }
                            
                        }




                    }
            }

            
            
    
        return toBeReturned;
    }
    
    
}



// extra notes 
//old version
// nodes.csv
            // regexChecker("[\"]{1}(\\w|\\s|\\W|[\\,]){0,}[\"]{1}[\\,]{1}|[\"]{0,}(\\w{0,}|\\s{0,1}|\\W{0,}){0,}[\"]{0,}[\\,]{1}|(\\w{0,}[\"]{0,1}|\\s|{0,1}|\\W{0,})",temp); 
            //[\"]{1}(\\w|\\s|\\W|[\\,]){0,}[\"]{1}[\\,]{1}                 part one 
            //[\"]{0,}(\\w{0,}|\\s{0,1}|\\W{0,}){0,}[\"]{0,}[\\,]{1}        part two
            